import { Module } from '@nestjs/common';
import { ClientsModule } from '@nestjs/microservices';
import { BroadcastService } from './aggregates/broadcast/broadcast.service';
import { eventsClient } from '../events.client';
import { BroadcastController } from './controllers/broadcast/broadcast.controller';

@Module({
  imports: [ClientsModule.registerAsync([eventsClient])],
  providers: [BroadcastService],
  controllers: [BroadcastController],
})
export class EventsModule {}
