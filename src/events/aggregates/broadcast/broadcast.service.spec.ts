import { Test, TestingModule } from '@nestjs/testing';
import { ConfigService } from '@nestjs/config';
import { BroadcastService } from './broadcast.service';
import { BROADCAST_EVENT } from '../../../events.client';

describe('BroadcastService', () => {
  let service: BroadcastService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BroadcastService,
        { provide: BROADCAST_EVENT, useValue: {} },
        { provide: ConfigService, useValue: {} },
      ],
    }).compile();

    service = module.get<BroadcastService>(BroadcastService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
