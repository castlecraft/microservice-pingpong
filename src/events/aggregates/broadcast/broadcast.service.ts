import { Injectable, Inject } from '@nestjs/common';
import { ClientMqtt } from '@nestjs/microservices';
import { ConfigService } from '@nestjs/config';
import { EVENTS_PATTERN } from '../../../config.options';
import { BROADCAST_EVENT } from '../../../events.client';

@Injectable()
export class BroadcastService {
  constructor(
    @Inject(BROADCAST_EVENT)
    private readonly broadcast: ClientMqtt,
    private readonly config: ConfigService,
  ) {}

  async emit() {
    await this.broadcast
      .emit(this.config.get(EVENTS_PATTERN), {
        message: this.config.get(EVENTS_PATTERN),
      })
      .toPromise();
  }
}
