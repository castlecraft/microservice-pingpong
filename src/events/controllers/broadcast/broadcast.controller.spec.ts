import { Test, TestingModule } from '@nestjs/testing';
import { BroadcastController } from './broadcast.controller';
import { BroadcastService } from '../../aggregates/broadcast/broadcast.service';

describe('BroadcastController', () => {
  let controller: BroadcastController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BroadcastController],
      providers: [{ provide: BroadcastService, useValue: {} }],
    }).compile();

    controller = module.get<BroadcastController>(BroadcastController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
