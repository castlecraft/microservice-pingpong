import { Controller, Post, Logger } from '@nestjs/common';
import { EventPattern, Payload } from '@nestjs/microservices';
import { BroadcastService } from '../../aggregates/broadcast/broadcast.service';

@Controller('broadcast')
export class BroadcastController {
  constructor(private readonly broadcast: BroadcastService) {}
  @Post('ping')
  async ping() {
    await this.broadcast.emit();
    return { message: 'pong' };
  }

  @EventPattern('PingEvent')
  pingEvent(@Payload() payload) {
    Logger.log(payload, this.constructor.name);
  }

  @EventPattern('PongEvent')
  pongEvent(@Payload() payload) {
    Logger.log(payload, this.constructor.name);
  }
}
