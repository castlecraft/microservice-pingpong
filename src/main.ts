import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';

import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import { PORT } from './config.options';
import { Logger } from '@nestjs/common';
import { useEventClientFactory } from './events.client';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const config = app.get<ConfigService>(ConfigService);
  const options = useEventClientFactory(config);
  const events = app.connectMicroservice(options);
  events.listen(() => {
    Logger.log('Listening Events', events.constructor.name);
  });
  await app.listen(Number(config.get(PORT)));
}
bootstrap();
