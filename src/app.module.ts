import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { configOptions } from './config.options';
import { EventsModule } from './events/events.module';

@Module({
  imports: [ConfigModule.forRoot(configOptions), EventsModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
