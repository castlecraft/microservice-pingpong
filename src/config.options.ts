import { ConfigModuleOptions } from '@nestjs/config/dist/interfaces';
import * as Joi from '@hapi/joi';

export const EVENTS_HOST = 'EVENTS_HOST';
export const EVENTS_PORT = 'EVENTS_PORT';
export const EVENTS_USER = 'EVENTS_USER';
export const EVENTS_PASSWORD = 'EVENTS_PASSWORD';
export const EVENTS_PATTERN = 'EVENTS_PATTERN';
export const PORT = 'PORT';

export const configOptions: ConfigModuleOptions = {
  isGlobal: true,
  validationSchema: Joi.object({
    NODE_ENV: Joi.string()
      .valid('development', 'production', 'test', 'staging')
      .default('development'),
    [EVENTS_HOST]: Joi.string().default('events'),
    [EVENTS_PORT]: Joi.string().default('1883'),
    [EVENTS_USER]: Joi.string().default('user'),
    [EVENTS_PASSWORD]: Joi.string().default('changeit'),
    [EVENTS_PATTERN]: Joi.string().default('PingEvent'),
    [PORT]: Joi.string().default('3000'),
  }),
};
