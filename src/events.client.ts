import { ConfigService } from '@nestjs/config';
import { MqttOptions, Transport } from '@nestjs/microservices';
import {
  EVENTS_HOST,
  EVENTS_PORT,
  EVENTS_USER,
  EVENTS_PASSWORD,
} from './config.options';

export const BROADCAST_EVENT = 'BROADCAST_EVENT';

export const useEventClientFactory = (config: ConfigService): MqttOptions => {
  const url = `mqtt://${config.get(EVENTS_USER)}:${config.get(
    EVENTS_PASSWORD,
  )}@${config.get(EVENTS_HOST)}:${config.get(EVENTS_PORT)}`;

  return {
    transport: Transport.MQTT,
    options: { url },
  };
};

export const eventsClient = {
  useFactory: useEventClientFactory,
  name: BROADCAST_EVENT,
  inject: [ConfigService],
};
