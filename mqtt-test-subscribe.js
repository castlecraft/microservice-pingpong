/* eslint no-console: "off" */
/* eslint @typescript-eslint/no-var-requires: "off" */

const mqtt = require('mqtt');

const quitMessage = 'Ctrl + C to quit';
const client = mqtt.connect(
  process.env.MQTT_URI || 'mqtt://user:changeit@localhost:1883',
);

client.on('connect', () => {
  console.info(`MQTT client connected. ${quitMessage}`);
});

client.subscribe('+', { qos: 0 });

client.on('message', (topic, message) => {
  console.info(`Topic: ${topic}`);
  console.info(message.toString());
  console.info(quitMessage);
  console.info();
});
