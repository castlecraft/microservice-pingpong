### Publish Event

```sh
# Copy and edit test-payload.json
cp test-payload-example.json test-payload.json
export EVENTS_PATTERN=MyTestEventTopic
node mqtt-test-publish.js
```

### Subscribe Event

```sh
node mqtt-test-subscribe.js
```
