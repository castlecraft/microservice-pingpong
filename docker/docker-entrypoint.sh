#!/bin/bash

function checkEnv() {
  if [[ -z "$EVENTS_HOST" ]]; then
    echo "EVENTS_HOST is not set"
    exit 1
  fi
  if [[ -z "$EVENTS_PORT" ]]; then
    echo "EVENTS_PORT is not set"
    exit 1
  fi
  if [[ -z "$EVENTS_USER" ]]; then
    echo "EVENTS_USER is not set"
    exit 1
  fi
  if [[ -z "$EVENTS_PASSWORD" ]]; then
    echo "EVENTS_PASSWORD is not set"
    exit 1
  fi
  if [[ -z "$EVENTS_PATTERN" ]]; then
    echo "EVENTS_PATTERN is not set"
    exit 1
  fi
  if [[ -z "$PORT" ]]; then
    echo "PORT is not set"
    exit 1
  fi
  if [[ -z "$NODE_ENV" ]]; then
    echo "NODE_ENV is not set"
    exit 1
  fi
}

function configureServer() {
  if [ ! -f .env ]; then
    envsubst '${NODE_ENV}
      ${EVENTS_HOST}
      ${EVENTS_PORT}
      ${EVENTS_USER}
      ${EVENTS_PASSWORD}
      ${EVENTS_PATTERN}
      ${PORT}' \
      < docker/env.tmpl > .env
  fi
}

export -f configureServer

if [ "$1" = 'start' ]; then
  # Validate environment variables
  checkEnv
  # Configure server
  su craft -c "bash -c configureServer"
  su craft -c "node dist/main.js"
fi

exec runuser -u craft "$@"
