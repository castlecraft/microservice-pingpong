FROM node:latest
# Copy and build app
COPY . /home/craft/microservice-pingpong
WORKDIR /home/craft/
RUN cd microservice-pingpong \
    && npm install \
    && npm run build \
    && rm -fr node_modules \
    && npm install --only=production

FROM node:slim
# Install dependencies
RUN apt-get update \
    && apt-get install -y gettext-base \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Setup docker-entrypoint
COPY docker/docker-entrypoint.sh usr/local/bin/docker-entrypoint.sh

# Add non root user and set home directory
RUN useradd -ms /bin/bash craft
WORKDIR /home/craft/microservice-pingpong
COPY --from=0 /home/craft/microservice-pingpong .
RUN chown -R craft:craft /home/craft

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["start"]
