/* eslint no-console: "off" */
/* eslint @typescript-eslint/no-var-requires: "off" */

const mqtt = require('mqtt');
const fs = require('fs');

let eventPattern = process.env.EVENTS_PATTERN;

if (!eventPattern) {
  eventPattern = 'PingEvent';
  console.warn(
    `EVENTS_PATTERN env variable not set. Using pattern: ${eventPattern}`,
  );
}

const client = mqtt.connect(
  process.env.MQTT_URI || 'mqtt://user:changeit@localhost:1883',
);

let payload = { message: 'ping' };

try {
  payload = JSON.parse(fs.readFileSync('./test-payload.json'));
} catch (error) {
  console.error(error.toString());
}

console.info(
  `Using payload: ${JSON.stringify(payload)} and pattern : ${eventPattern}`,
);

client.on('connect', () => {
  client.subscribe(eventPattern, err => {
    if (!err) {
      client.publish(eventPattern, JSON.stringify(payload));
    }
    client.end();
  });
});
